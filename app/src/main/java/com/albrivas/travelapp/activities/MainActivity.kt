package com.albrivas.travelapp.activities

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.albrivas.mylibrary.ToolbarActivity
import com.albrivas.travelapp.R
import com.albrivas.travelapp.fragments.ArrivalsFragment
import com.albrivas.travelapp.fragments.DeparturesFragment
import com.albrivas.travelapp.fragments.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header.*
import org.jetbrains.anko.toast


class MainActivity : ToolbarActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme) // Splash Screen
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbarToLoad(toolbarItem as Toolbar?)
        setNavdrawer()
        setUserHeaderInformation()
        fragmentTransaction(HomeFragment())
        navView.menu.getItem(0).isChecked = true
    }

    private fun setNavdrawer() {
        val toggle = ActionBarDrawerToggle(this, drawer, toolbarItem as Toolbar?,
            R.string.open_drawer,
            R.string.close_drawer
        )
        toggle.isDrawerIndicatorEnabled = true
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
    }

    private fun fragmentTransaction(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }

    private fun loadFragmentById(id: Int) {
        when(id) {
            R.id.nav_home -> fragmentTransaction(HomeFragment())
            R.id.nav_arrivals -> fragmentTransaction(ArrivalsFragment())
            R.id.nav_departure -> fragmentTransaction(DeparturesFragment())
        }
    }

    private fun showMessageNavItemSelectedById(id: Int) {
        when(id) {
            R.id.nav_profile -> toast("")
            R.id.nav_settings -> toast("")
        }
    }

    private fun closeDrawer() {
        drawer.closeDrawer(GravityCompat.START)
    }

    private fun setUserHeaderInformation() {
        val name = textUserName
        val email = textUserEmail

        // let -> If name and email are not null
        name?.let {  name.text = getString(R.string.user_name) }
        email?.let { email.text = getString(R.string.user_email) }
    }

    //region OVERRIDE
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var id = item.itemId

        loadFragmentById(id)
        showMessageNavItemSelectedById(id)
        closeDrawer()

        return true
    }

    override fun onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            closeDrawer()
        }
        super.onBackPressed()
    }

    //endregion

}
